/* Store backend based on block devices offered by rump kernel drivers.

   Copyright (C) 2017 Luca Dariz <luca@orpolo.org>

   The GNU Hurd is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2, or (at
   your option) any later version.

   The GNU Hurd is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111, USA. */

#define _GNU_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <errno.h>

#include <hurd.h>
#include <hurd/store.h>

#include <rump/rump.h>
#include <rump/rump_syscalls.h>
#include <rump/rumperrno2host.h>

/* To use ioctls without messing up with netbsd sys/ */
#include "blockrump_netbsd.h"

static bool rump_init_done=false;

struct rump_store_info
{
  int fd;
};

extern const struct store_class store_blockrump_class;

static error_t dopen(const char *name, int *fd)
{
  error_t err=0;
  *fd = rump_sys_open(name, RUMP_O_RDWR);
  if (*fd < 0)
    {
      err = rump_errno2host(errno);
    }
  return err;
}

static error_t
blockrump_read (struct store *store, store_offset_t addr, size_t index,
		mach_msg_type_number_t amount, void **buf,
		mach_msg_type_number_t *len)
{
  int ret;
  struct rump_store_info *misc = (struct rump_store_info*)store->misc;

  if (misc->fd < 0)
    return EBADF;

  addr = addr*store->block_size;

  ret = rump_sys_lseek(misc->fd, addr, SEEK_SET);
  if (ret < 0)
    {
      return rump_errno2host(errno);
    }

  if (*len < amount)
    {
      *buf = mmap (0, amount, PROT_READ|PROT_WRITE, MAP_ANON, 0, 0);
      if (*buf == MAP_FAILED)
	{
	  ret = ENOMEM;
	  return ret;
	}
    }

  ret = rump_sys_read(misc->fd, *buf, amount);
  if (ret > 0)
    {
      *len = ret;
      ret = 0;
    }
  else
    {
      ret = rump_errno2host(errno);
    }

  return ret;
}

static error_t
blockrump_write (struct store *store,
		 store_offset_t addr, size_t index,
		 const void *buf, mach_msg_type_number_t len,
		 mach_msg_type_number_t *amount)
{
  int ret;
  struct rump_store_info *misc = (struct rump_store_info*)store->misc;

  if (misc->fd < 0)
    return EBADF;

  addr = addr*store->block_size;

  ret = rump_sys_lseek(misc->fd, addr, SEEK_SET);
  if (ret < 0)
    {
      return rump_errno2host(errno);
    }

  ret = rump_sys_write(misc->fd, buf, len);
  if (ret > 0)
    {
      *amount = ret;
      ret = 0;
    }
  else
    {
      ret = rump_errno2host(errno);
    }
  return ret;
}

static error_t
blockrump_set_size (struct store *store, size_t newsize)
{
  return EOPNOTSUPP;
}

static error_t
_store_blockrump_create (device_t device, int flags, size_t block_size,
			 const struct store_run *runs, size_t num_runs,
			 struct store **store)
{
  return
    _store_create (&store_blockrump_class, device, flags, block_size,
		   runs, num_runs, 0, store);
}

static error_t
blockrump_decode (struct store_enc *enc,
		  const struct store_class *const *classes,
		  struct store **store)
{
  return store_std_leaf_decode (enc, _store_blockrump_create, store);
}

static error_t
blockrump_open (const char *name, int flags,
		const struct store_class *const *classes,
		struct store **store)
{
  size_t block_size;
  struct store_run run;
  int fd;
  error_t err=0;

  if (!rump_init_done)
    {
      fd = rump_init();
      if (fd != 0)
	return EPERM;
      else
	rump_init_done = true;
    }

  err = dopen(name, &fd);
  if (err)
    return err;

  off_t size;
  if (rump_sys_ioctl(fd, DIOCGMEDIASIZE, &size) != 0)
    {
      rump_sys_close(fd);
      return rump_errno2host(errno);
    }

  u_int ssize;
  if (rump_sys_ioctl(fd, DIOCGSECTORSIZE, &ssize) != 0)
    {
      rump_sys_close(fd);
      return rump_errno2host(errno);
    }

  block_size = ssize;
  run.start = 0;
  run.length = size/block_size;

  flags |= STORE_ENFORCED;	/* 'cause it's the whole device.  */

  err = _store_create (&store_blockrump_class, MACH_PORT_NULL, flags,
		       block_size, &run, 1, 0, store);

  if (! err)
    {
      struct rump_store_info *misc;
      
      (*store)->port = MACH_PORT_NULL;
      misc = malloc(sizeof(struct rump_store_info));
      if (misc)
	{
	  misc->fd = fd;
	  (*store)->misc = misc;;
	  (*store)->misc_len = sizeof(struct rump_store_info);
	}
      else
	{
	  err = ENOMEM;
	}
    }

  if (! err)
    {
      err = store_set_name (*store, name);
      if (err)
	{
	  store_free (*store);
	}
    }

  if (err)
    rump_sys_close(fd);

  return err;
}

static void
blockrump_cleanup (struct store *store)
{
  struct rump_store_info *misc = (struct rump_store_info*)store->misc;
  rump_sys_close(misc->fd);
  free(store->misc);
  store->misc = NULL;
}

static error_t
blockrump_set_flags (struct store *store, int flags)
{
  if ((flags & ~(STORE_INACTIVE | STORE_ENFORCED)) != 0)
    /* Trying to set flags we don't support.  */
    return EINVAL;

  if (flags & STORE_INACTIVE)
    {
      struct rump_store_info *misc = (struct rump_store_info*)store->misc;
      rump_sys_close(misc->fd);
    }

  store->flags |= flags;

  return 0;
}

static error_t
blockrump_clear_flags (struct store *store, int flags)
{
  error_t err = 0;
  /* ENFORCED should probably not be cleared */
  if ((flags & ~(STORE_INACTIVE)) != 0)
    err = EINVAL;

  if (!err && (flags & STORE_INACTIVE))
    {
      struct rump_store_info *misc = (struct rump_store_info*)store->misc;
      err = store->name ? dopen (store->name, &misc->fd) : ENODEV;
    }

  if (! err)
    store->flags &= ~flags;

  return err;
}

static error_t
blockrump_map (const struct store *store, vm_prot_t prot, mach_port_t *memobj)
{
  return EOPNOTSUPP;
}

const struct store_class
store_blockrump_class =
  {
    STORAGE_OTHER, "blockrump",
    blockrump_read,
    blockrump_write,
    blockrump_set_size,
    store_std_leaf_allocate_encoding,
    store_std_leaf_encode,
    blockrump_decode,
    blockrump_set_flags,
    blockrump_clear_flags,
    blockrump_cleanup,
    0,
    0,
    blockrump_open,
    0,
    blockrump_map
  };
STORE_STD_CLASS (blockrump);
