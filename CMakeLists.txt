cmake_minimum_required(VERSION 2.8.9)
project(blockrump)
set(CMAKE_BUILD_TYPE Release)

set(SOURCES blockrump.c)
add_library(store_blockrump SHARED ${SOURCES})

set(LIBS -lrump -lrumpuser -lrumpdev -lrumpdev_disk -lrumpdev_pci
  -lrumpdev_pci_virtio -lrumpdev_virtio_ld -lrumpvfs)
target_link_libraries(store_blockrump ${LIBS})

# libtool doesn't allow me to specify a soname with version 0.3 (hurd version)
# needed to be used as libstore backend AND to be found by ldconfig
set_property(TARGET store_blockrump PROPERTY VERSION 0.3)
set_property(TARGET store_blockrump PROPERTY SOVERSION 0.3 )

# Apparently cmake doesn't know hot to enable large file support
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -D_FILE_OFFSET_BITS=64")

# add make install target
install(TARGETS store_blockrump DESTINATION /usr/local/lib)

# add make uninstall target
configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/cmake_uninstall.cmake.in"
  "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake"
  IMMEDIATE @ONLY)
add_custom_target(uninstall
  COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake)
